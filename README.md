# Neverball map editor support

This is a gamepack for [NetRadiant](https://netradiant.gitlab.io) providing support for Neverball.

This gamepack is based on the game pack provided by Ingar:

- http://ingar.intranifty.net/files/netradiant/gamepacks/NeverballPack.zip
